/**
 * Created by Nina on 08.06.2017.
 */
public class Node {
    private int position;
    private Node prev;
    private Node next;

    public Node(int position) {
        this.position = position;
        this.prev = null;
        this.next = null;
    }

    public int getPosition() {
        return position;
    }


    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
