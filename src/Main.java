

/**
 * Created by Nina on 08.06.2017.
 * Objaśnienie:
 Flawiusz Josephus był historykiem żyjącym w I w. Legenda głosi, że gdyby nie jego talent ma-tematyczny, nie dożyłby chwili gdy został sławny. W czasie wojny żydowsko-rzymskiej był on w oddziale 41 (czyli „N”) żydowskich buntowników osaczonych przez Rzymian. Woląc śmierć od niewoli, buntownicy stanęli w kole, odliczając i zabijając co trzecią (co „K-tą”) osobę. Jednak Flawiusz, wraz ze swym przyjacielem, chcieli uniknąć takiej śmierci. Flawiusz szybko obliczył gdzie powinni obaj stanąć, aby byli ostatnimi dwiema osobami, które zostaną przy życiu.

 Program dla zadanych dwóch liczb: N oraz K (jak w objaśnieniu), wyznacza początkową pozycję dwóch elementów listy, które pozostaną w powtarzanym cyklicznie procesie usuwania co K-tego elementu listy N-elementowej, rozpoczynając od 1. elementu. Należy też wyświetlić kolejne usuwane elementy listy.

 Uwaga zaimplementować BEZ wykorzystania wzoru!!!.
 */
public class Main {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue(25);
        myQueue.hitARound(myQueue.getHead(),1,4);



    }


}
