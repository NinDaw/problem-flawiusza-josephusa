import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nina on 08.06.2017.
 */
public class MyQueue {
    private List<Node> list;
    private Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    /**
     * Utworzenie listy i powiązań pomiędzy kolejnymi pozycjami na liście
     * @param N to ilość pozycji
     */
    public MyQueue(int N) {
        this.list = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            list.add(new Node(i));
        }
        if (N > 1) {
            for (Node node : list) {
                if (list.indexOf(node) == 0) {
                    this.head = node;
                    head.setNext(list.get(1));
                    head.setPrev(list.get(list.size() - 1));
                } else if (list.indexOf(node) == list.size() - 1) {
                    this.tail = node;
                    int prev = list.indexOf(node) - 1;
                    tail.setPrev(list.get(prev));
                    tail.setNext(head);
                } else {
                    int prev = list.indexOf(node) - 1;
                    int next = list.indexOf(node) + 1;
                    node.setNext(list.get(next));
                    node.setPrev(list.get(prev));
                }
            }
        } else if (N == 1) {
            this.head = list.get(0);
            this.tail = null;
        } else {
            this.head = null;
            this.tail = null;
        }

    }

    /**
     * Metoda odnajdująca 2 ostatnie pozycje z listy
     * @param start punkt od którego rozpoczęto odliczanie
     * @param i licznik
     * @param K
     */
    public void hitARound(Node start, int i, int K) {
        if (list.size() > 2) {
            if (i < K) {
                Node newStart = start.getNext();
                i++;
                hitARound(newStart, i, K);
            } else if (i == K) {
                Node temp = start.getNext();
                removeNode(start);
                System.out.println("Usunięto "+start.getPosition());
                start = temp;
                i = 1;
                hitARound(start, i, K);
            }
        }else{
        System.out.println("Zostały tylko " + list.size() + " nody.");
        for (Node node: list){
            System.out.println(node.getPosition());}
        }
    }

    /**
     * Usuwanie Noda z listy
     * @param node jest usuwany z listy
     */
    public void removeNode(Node node) {
        Node prev = node.getPrev();
        Node next = node.getNext();
        prev.setNext(next);
        next.setPrev(prev);
        list.remove(node);
    }

}
